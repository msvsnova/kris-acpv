#------------------------------------------------------------------------------
#
# SuperNova APPLICATION - ACLOG - MAKEFILE
# By FIST -  Kresimir Mandic 27.03.1999
#
#------------------------------------------------------------------------------

# Compiler options
#------------------------------------------------------------------------------
## CC          = start /w c:\snova\sncd\nova
CC          = start /w c:\vscd\bin\nova


CFLAGS      = -w -ereadfgl
TD			= X:\acpv\ 
#TD			= T:\acpv\ 
COMD        = .\lib\acpvcommon.fle 


ACFILES  =  $(TD)aca_dd.fle     $(TD)aca_flds.fle      $(TD)acpv.fle         \
			$(TD)vkatalog.fle   $(TD)vcjenik.fle       $(TD)vozmskul.fle     \
			$(TD)asci_aca.fle   $(TD)voz_stav.fle      $(TD)lvpotvrd.fle     \
			$(TD)dealer.fle     $(TD)prim_zg.fle       $(TD)pr_st_zg.fle     \
			$(TD)prvozila.fle   $(TD)povlppar.fle      $(TD)acadbutl.fle     \
			$(TD)lotpvoz.fle    $(TD)exterier.fle      $(TD)interier.fle     \
			$(TD)prvozobr.fle   $(TD)lprimvoz.fle      $(TD)lprim_gr.fle     \
			$(TD)vozilagr.fle   $(TD)vozst_gr.fle      $(TD)lponvoz.fle      \
			$(TD)lpnalog.fle    $(TD)vpovlast.fle      $(TD)voz_rab.fle      \
			$(TD)lprvoz_p1.fle  $(TD)lprvoz_p2.fle     $(TD)lprvoz_p3.fle    \
			$(TD)l_cjenik.fle   $(TD)lkatalog.fle      $(TD)prvoztot.fle     \
			$(TD)ac_specp.fle   $(TD)lotpmalp.fle      $(TD)boje.fle     \
			$(TD)prvozfak.fle   $(TD)lotpizvoz.fle     $(TD)vkathomo.fle     \
			$(TD)vozilazg.fle   $(TD)l_izosug.fle      $(TD)vstd_op.fle      \
			$(TD)lrnfacro.fle   $(TD)izborpod.fle      $(TD)voz_izvj.fle     \
			$(TD)lvozfakt.fle   $(TD)vozopstd.fle      $(TD)vtrosar_eu.fle   \
			$(TD)voz_stdl.fle   $(TD)voz_msk.fle       $(TD)vozmskst.fle     \
			$(TD)lvoz_msk.fle   $(TD)lvozlager.fle     $(TD)prim_cs.fle      \
			$(TD)pr_st_cs.fle   $(TD)primcszg.fle      $(TD)prstcszg.fle     \
			$(TD)specdrab.fle   $(TD)kart_voz.fle      $(TD)lfaktdil.fle     \
			$(TD)facrourz.fle   $(TD)aca_ascii.fle     $(TD)vozmjesta.fle    \
			$(TD)vozobrac.fle   $(TD)vozponobr.fle     $(TD)voz_nzp.fle      \
			$(TD)voznzpst.fle   $(TD)vnarfiat.fle      $(TD)vnfiatst.fle     \
			$(TD)vozdepr.fle    $(TD)lulkzap.fle       $(TD)vozizdcs.fle     \
			$(TD)fiatfile.fle   $(TD)lrazkzap.fle      $(TD)lzalkzap.fle     \
			$(TD)lizkzap.fle    $(TD)vtrosar.fle       $(TD)vozilagr2.fle    \
			$(TD)lspjcduz.fle   $(TD)lizvkzap.fle      $(TD)vozjcdul.fle     \
			$(TD)vozjcdca.fle   $(TD)razlkzap.fle      $(TD)lotpyugo.fle     \
			$(TD)aca_dbf.fle    $(TD)vozsalon.fle      $(TD)vozsalag.fle     \
			$(TD)ppardeal.fle   $(TD)vozdelag.fle      $(TD)lvnspecd.fle     \
			$(TD)lacspecp.fle   $(TD)lspdrab.fle       $(TD)ljcduzgo.fle     \
			$(TD)acnarfiat.fle  $(TD)vnstdop.fle       $(TD)lvoznarac.fle    \
			$(TD)vnfambdg.fle   $(TD)lvnfambdg.fle     $(TD)vozexcel.fle     \
			$(TD)drabati.fle    $(TD)ldrabati.fle      $(TD)ldrabtot.fle     \
			$(TD)vozinlst.fle   $(TD)vbrnarac.fle      $(TD)chgprice.fle     \
            $(TD)lchgpric.fle   $(TD)voz_pst.fle       $(TD)proldvoz.fle     \
            $(TD)old_vst.fle    $(TD)kontkzap.fle      $(TD)vozgr_sa.fle     \
            $(TD)lvozdnev.fle   $(TD)lbosnatpc.fle     $(TD)vozsklad.fle     \
            $(TD)lfvfmbdg.fle   $(TD)vozfaktdo.fle     $(TD)vcjenkod.fle     \
            $(TD)vozila_ps.fle  $(TD)lkzap_car.fle     $(TD)vozakcija.fle    \
            $(TD)vozaddkup.fle  $(TD)transcje.fle      $(TD)transpfak.fle    \
          $(TD)vtros_benzin.fle $(TD)vtros_diesel.fle 

ACFILES2 =  $(TD)trans_voz.fle  $(TD)transotp.fle      $(TD)trans_dest.fle   \
            $(TD)trans_pri.fle  $(TD)trans_routes.fle  $(TD)l_transcje.fle   \
            $(TD)l_trans_pri.fle $(TD)ltrans_routes.fle $(TD)transtat.fle    \
            $(TD)l_trans_dest.fle $(TD)transtotp.fle   $(TD)l_transotp.fle   \
            $(TD)l_trans_voz.fle  $(TD)l_transpfak.fle $(TD)l_transpfak2.fle \
            $(TD)l_transtat.fle $(TD)trans_burza.fle   $(TD)l_pvfmbdg.fle    \
            $(TD)l_pvozs.fle    $(TD)route_dest.fle    $(TD)l_route_dest.fle \
            $(TD)l_trans_cmr.fle  $(TD)trans_cmr.fle   $(TD)trans_psklad.fle \
            $(TD)ltrans_psklad.fle $(TD)voztipsp.fle   $(TD)vozdealsp.fle    \
            $(TD)vozkatdo.fle   $(TD)vozdosin.fle      $(TD)shcdodop.fle     \
            $(TD)l_vozkatdo.fle $(TD)l_vozdosin.fle    $(TD)l_shcdodop.fle   \
            $(TD)fmbdgweb.fle   $(TD)homo_var.fle      $(TD)acpv_idx.fle     \
            $(TD)salonoff.fle   $(TD)vrs_nzk.fle       $(TD)vozuzak.fle      \
            $(TD)vozizak.fle    $(TD)nzk.fle           $(TD)nktipknj.fle     \
            $(TD)nzk_inousl.fle $(TD)nzk_coun.fle      $(TD)spec_ob14.fle    \
            $(TD)nzk_ob14.fle   $(TD)l_nzk.fle         $(TD)salonfak.fle     \
            $(TD)nzk_wbspec.fle $(TD)voz_napl.fle      $(TD)voz_kup_ascii.fle \
            $(TD)salonxls.fle   $(TD)spec_ob14z.fle    $(TD)util_nzk.fle      \
            $(TD)lprimcszg.fle  $(TD)ls_pvfmbdg.fle    $(TD)salonde.fle       \
            $(TD)okzap_car.fle  $(TD)l_nzk_roba.fle    $(TD)trans_cmrKA.fle   \
            $(TD)l_trans_cmrKA.fle  $(TD)lponvoz_kn.fle $(TD)l_izosug_new.fle \
            $(TD)lkzap_kont.fle $(TD)voz_finlag.fle    $(TD)l_finlag.fle      \
            $(TD)finlag_pon.fle $(TD)lpon_finlag.fle   $(TD)azur_stav.fle     \
            $(TD)chg_ordine.fle $(TD)lkzapkn_car.fle   $(TD)cflimiti.fle      \
            $(TD)lkzapeur_car.fle $(TD)dkzap_car.fle   $(TD)crediflex.fle     \
            $(TD)l_ispod_nab.fle $(TD)l_voz_co2.fle    $(TD)ctar_hist.fle     \
            $(TD)vozupdtc.fle   $(TD)vrste_prodaje.fle $(TD)opisnik.fle       \
            $(TD)eucjenik.fle   $(TD)skupine_op.fle    $(TD)cszg_kalk.fle     \
            $(TD)cszg_kalks.fle $(TD)lotpvoz_eu.fle    $(TD)lotpmalp_eu.fle   \
            $(TD)ltrosarine.fle $(TD)acpv_rev.fle      $(TD)salon.fle         \
            $(TD)salnarfiat.fle $(TD)l_ngo.fle  $(TD)chg_kodeks.fle \
            $(TD)l_rabcjenik.fle $(TD)l_eucjenik.fle $(TD)l_eucjenik8.fle  \
            $(TD)l_eucjenik7.fle $(TD)l_eucjenik6.fle $(TD)l_eucjenik5.fle   \
            $(TD)l_eucjenik4.fle $(TD)l_eucjenik10.fle $(TD)l_eucjenik11.fle \
            $(TD)l_jamstvo5.fle $(TD)prodavaci.fle $(TD)chkcijene.fle \
            $(TD)chkcijene_st.fle $(TD)lponvoz_en.fle $(TD)tros_xml.fle \
            $(TD)tekstovi.fle $(TD)vmarke.fle

COMMON    = aca_dd.ls aca_ddx.ls aca_flds.ls dealer.ls exterier.ls interier.ls \
            prvozfak.ls prvozila.ls prvozobr.ls vozizak.ls vozuzak.ls vozjcdca.ls \
            vozjcdul.ls voz_napl.ls vpovlast.ls
            

all      : $(ACFILES) $(ACFILES2) 

## COMMON

## aca_dd.ls:  aca_dd.lgc
    ## $(CC) $(CFLAGS) appl=aca_dd dotfle=.\lib\aca_dd.fle 
    ## @echo " ">aca_dd.ls
## 
## aca_ddx.ls:  aca_ddx.lgc
    ## $(CC) $(CFLAGS) appl=aca_ddx dotfle=.\lib\aca_dd.fle 
    ## @echo " ">aca_ddx.ls
## 
## aca_flds.ls:  aca_flds.lgc
    ## $(CC) $(CFLAGS) appl=aca_flds dotfle=.\lib\aca_dd.fle 
    ## @echo " ">aca_flds.ls
## 
## dealer.ls:  dealer.lgc
    ## $(CC) $(CFLAGS) appl=dealer dotfle=$(COMD)
    ## @echo " ">dealer.ls
## 
## exterier.ls:  exterier.lgc
    ## $(CC) $(CFLAGS) appl=exterier dotfle=$(COMD)
    ## @echo " ">exterier.ls
## 
## interier.ls:  interier.lgc
    ## $(CC) $(CFLAGS) appl=interier dotfle=$(COMD)
    ## @echo " ">interier.ls
## 
## prvozfak.ls:  prvozfak.lgc
    ## $(CC) $(CFLAGS) appl=prvozfak dotfle=$(COMD)
    ## @echo " ">prvozfak.ls
## 
## prvozila.ls:  prvozila.lgc
    ## $(CC) $(CFLAGS) appl=prvozila dotfle=$(COMD)
    ## @echo " ">prvozila.ls
## 
## vozizak.ls:  vozizak.lgc
    ## $(CC) $(CFLAGS) appl=vozizak dotfle=$(COMD)
    ## @echo " ">vozizak.ls
## 
## vozuzak.ls:  vozuzak.lgc
    ## $(CC) $(CFLAGS) appl=vozuzak dotfle=$(COMD)
    ## @echo " ">vozuzak.ls
## 
## prvozobr.ls:  prvozobr.lgc
    ## $(CC) $(CFLAGS) appl=prvozobr dotfle=$(COMD)
    ## @echo " ">prvozobr.ls
## 
## vozjcdca.ls:  vozjcdca.lgc
    ## $(CC) $(CFLAGS) appl=vozjcdca dotfle=$(COMD)
    ## @echo " ">vozjcdca.ls
## 
## vozjcdul.ls:  vozjcdul.lgc
    ## $(CC) $(CFLAGS) appl=vozjcdul dotfle=$(COMD)
    ## @echo " ">vozjcdul.ls
## 
## voz_napl.ls:  voz_napl.lgc
    ## $(CC) $(CFLAGS) appl=voz_napl dotfle=$(COMD)
    ## @echo " ">voz_napl.ls
## 
## vpovlast.ls:  vpovlast.lgc
    ## $(CC) $(CFLAGS) appl=vpovlast dotfle=$(COMD)
    ## @echo " ">vpovlast.ls

#########################3

$(TD)aca_dd.fle:  aca_dd.lgc
    $(CC) $(CFLAGS) appl=aca_dd dotfle=$(TD)aca_dd.fle
    $(CC) -w genddx ddfile=aca_dd
    $(CC) $(CFLAGS) appl=aca_ddx dotfle=$(TD)aca_dd.fle

$(TD)aca_flds.fle:  aca_flds.lgc
    $(CC) $(CFLAGS) appl=aca_flds dotfle=$(TD)aca_flds.fle

$(TD)acadbutl.fle:  acadbutl.lgc
    $(CC) $(CFLAGS) appl=acadbutl dotfle=$(TD)acadbutl.fle

$(TD)vozilagr2.fle:  vozilagr2.lgc
    $(CC) $(CFLAGS) appl=vozilagr2 dotfle=$(TD)vozilagr2.fle

$(TD)acpv.fle:  acpv.lgc
    $(CC) $(CFLAGS) appl=acpv dotfle=$(TD)acpv.fle

$(TD)vkatalog.fle:  vkatalog.lgc
    $(CC) $(CFLAGS) appl=vkatalog dotfle=$(TD)vkatalog.fle

$(TD)vcjenik.fle:  vcjenik.lgc
    $(CC) $(CFLAGS) appl=vcjenik dotfle=$(TD)vcjenik.fle

$(TD)asci_aca.fle:  asci_aca.lgc
    $(CC) $(CFLAGS) appl=asci_aca dotfle=$(TD)asci_aca.fle

$(TD)aca_dbf.fle:  aca_dbf.lgc
    $(CC) $(CFLAGS) appl=aca_dbf dotfle=$(TD)aca_dbf.fle

$(TD)voz_stav.fle:  voz_stav.lgc
    $(CC) $(CFLAGS) appl=voz_stav dotfle=$(TD)voz_stav.fle

$(TD)dealer.fle:  dealer.lgc
    $(CC) $(CFLAGS) appl=dealer dotfle=$(TD)dealer.fle

$(TD)prim_zg.fle:  prim_zg.lgc
    $(CC) $(CFLAGS) appl=prim_zg dotfle=$(TD)prim_zg.fle

$(TD)pr_st_zg.fle:  pr_st_zg.lgc
    $(CC) $(CFLAGS) appl=pr_st_zg dotfle=$(TD)pr_st_zg.fle

$(TD)prvozila.fle:  prvozila.lgc
    $(CC) $(CFLAGS) appl=prvozila dotfle=$(TD)prvozila.fle

$(TD)povlppar.fle:  povlppar.lgc
    $(CC) $(CFLAGS) appl=povlppar dotfle=$(TD)povlppar.fle

$(TD)lotpvoz.fle:  lotpvoz.lgc
    $(CC) $(CFLAGS) appl=lotpvoz dotfle=$(TD)lotpvoz.fle

$(TD)exterier.fle:  exterier.lgc
    $(CC) $(CFLAGS) appl=exterier dotfle=$(TD)exterier.fle

$(TD)interier.fle:  interier.lgc
    $(CC) $(CFLAGS) appl=interier dotfle=$(TD)interier.fle

$(TD)prvozobr.fle:  prvozobr.lgc
    $(CC) $(CFLAGS) appl=prvozobr dotfle=$(TD)prvozobr.fle

$(TD)lprimvoz.fle:  lprimvoz.lgc
    $(CC) $(CFLAGS) appl=lprimvoz dotfle=$(TD)lprimvoz.fle

$(TD)lprim_gr.fle:  lprim_gr.lgc
    $(CC) $(CFLAGS) appl=lprim_gr dotfle=$(TD)lprim_gr.fle

$(TD)vozilagr.fle:  vozilagr.lgc
    $(CC) $(CFLAGS) appl=vozilagr dotfle=$(TD)vozilagr.fle

$(TD)vozst_gr.fle:  vozst_gr.lgc
    $(CC) $(CFLAGS) appl=vozst_gr dotfle=$(TD)vozst_gr.fle

$(TD)lponvoz.fle:  lponvoz.lgc
    $(CC) $(CFLAGS) appl=lponvoz dotfle=$(TD)lponvoz.fle

$(TD)lponvoz_kn.fle:  lponvoz_kn.lgc
    $(CC) $(CFLAGS) appl=lponvoz_kn dotfle=$(TD)lponvoz_kn.fle

$(TD)lponvoz_en.fle:  lponvoz_en.lgc
    $(CC) $(CFLAGS) appl=lponvoz_en dotfle=$(TD)lponvoz_en.fle

$(TD)lpnalog.fle:  lpnalog.lgc
    $(CC) $(CFLAGS) appl=lpnalog dotfle=$(TD)lpnalog.fle

$(TD)vpovlast.fle:  vpovlast.lgc
    $(CC) $(CFLAGS) appl=vpovlast dotfle=$(TD)vpovlast.fle

$(TD)voz_rab.fle:  voz_rab.lgc
    $(CC) $(CFLAGS) appl=voz_rab dotfle=$(TD)voz_rab.fle

$(TD)lprvoz_p1.fle:  lprvoz_p1.lgc
    $(CC) $(CFLAGS) appl=lprvoz_p1 dotfle=$(TD)lprvoz_p1.fle

$(TD)lprvoz_p2.fle:  lprvoz_p2.lgc
    $(CC) $(CFLAGS) appl=lprvoz_p2 dotfle=$(TD)lprvoz_p2.fle

$(TD)lprvoz_p3.fle:  lprvoz_p3.lgc
    $(CC) $(CFLAGS) appl=lprvoz_p3 dotfle=$(TD)lprvoz_p3.fle

$(TD)l_cjenik.fle:  l_cjenik.lgc
    $(CC) $(CFLAGS) appl=l_cjenik dotfle=$(TD)l_cjenik.fle

$(TD)lkatalog.fle:  lkatalog.lgc
    $(CC) $(CFLAGS) appl=lkatalog dotfle=$(TD)lkatalog.fle

$(TD)prvoztot.fle:  prvoztot.lgc
    $(CC) $(CFLAGS) appl=prvoztot dotfle=$(TD)prvoztot.fle

$(TD)ac_specp.fle:  ac_specp.lgc
    $(CC) $(CFLAGS) appl=ac_specp dotfle=$(TD)ac_specp.fle

$(TD)lotpmalp.fle:  lotpmalp.lgc
    $(CC) $(CFLAGS) appl=lotpmalp dotfle=$(TD)lotpmalp.fle

$(TD)prvozfak.fle:  prvozfak.lgc
    $(CC) $(CFLAGS) appl=prvozfak dotfle=$(TD)prvozfak.fle

$(TD)lotpizvoz.fle:  lotpizvoz.lgc
    $(CC) $(CFLAGS) appl=lotpizvoz dotfle=$(TD)lotpizvoz.fle

$(TD)vkathomo.fle:  vkathomo.lgc
    $(CC) $(CFLAGS) appl=vkathomo dotfle=$(TD)vkathomo.fle

$(TD)vozilazg.fle:  vozilazg.lgc
    $(CC) $(CFLAGS) appl=vozilazg dotfle=$(TD)vozilazg.fle

$(TD)l_izosug.fle:  l_izosug.lgc
    $(CC) $(CFLAGS) appl=l_izosug dotfle=$(TD)l_izosug.fle

$(TD)l_izosug_new.fle:  l_izosug_new.lgc
    $(CC) $(CFLAGS) appl=l_izosug_new dotfle=$(TD)l_izosug_new.fle

$(TD)vstd_op.fle:  vstd_op.lgc
    $(CC) $(CFLAGS) appl=vstd_op dotfle=$(TD)vstd_op.fle

$(TD)lrnfacro.fle:  lrnfacro.lgc
    $(CC) $(CFLAGS) appl=lrnfacro dotfle=$(TD)lrnfacro.fle

$(TD)izborpod.fle:  izborpod.lgc
    $(CC) $(CFLAGS) appl=izborpod dotfle=$(TD)izborpod.fle

$(TD)voz_izvj.fle:  voz_izvj.lgc
    $(CC) $(CFLAGS) appl=voz_izvj dotfle=$(TD)voz_izvj.fle

$(TD)lvozfakt.fle:  lvozfakt.lgc
    $(CC) $(CFLAGS) appl=lvozfakt dotfle=$(TD)lvozfakt.fle

$(TD)vozopstd.fle:  vozopstd.lgc
    $(CC) $(CFLAGS) appl=vozopstd dotfle=$(TD)vozopstd.fle

$(TD)voz_stdl.fle:  voz_stdl.lgc
    $(CC) $(CFLAGS) appl=voz_stdl dotfle=$(TD)voz_stdl.fle

$(TD)voz_msk.fle:  voz_msk.lgc
    $(CC) $(CFLAGS) appl=voz_msk dotfle=$(TD)voz_msk.fle

$(TD)vozmskst.fle:  vozmskst.lgc
    $(CC) $(CFLAGS) appl=vozmskst dotfle=$(TD)vozmskst.fle

$(TD)lvoz_msk.fle:  lvoz_msk.lgc
    $(CC) $(CFLAGS) appl=lvoz_msk dotfle=$(TD)lvoz_msk.fle

$(TD)lvozlager.fle:  lvozlager.lgc
    $(CC) $(CFLAGS) appl=lvozlager dotfle=$(TD)lvozlager.fle

$(TD)prim_cs.fle:  prim_cs.lgc
    $(CC) $(CFLAGS) appl=prim_cs dotfle=$(TD)prim_cs.fle

$(TD)pr_st_cs.fle:  pr_st_cs.lgc
    $(CC) $(CFLAGS) appl=pr_st_cs dotfle=$(TD)pr_st_cs.fle

$(TD)primcszg.fle:  primcszg.lgc
    $(CC) $(CFLAGS) appl=primcszg dotfle=$(TD)primcszg.fle

$(TD)prstcszg.fle:  prstcszg.lgc
    $(CC) $(CFLAGS) appl=prstcszg dotfle=$(TD)prstcszg.fle

$(TD)specdrab.fle:  specdrab.lgc
    $(CC) $(CFLAGS) appl=specdrab dotfle=$(TD)specdrab.fle

$(TD)vozmskul.fle:  vozmskul.lgc
    $(CC) $(CFLAGS) appl=vozmskul dotfle=$(TD)vozmskul.fle

$(TD)kart_voz.fle:  kart_voz.lgc
    $(CC) $(CFLAGS) appl=kart_voz dotfle=$(TD)kart_voz.fle

$(TD)lfaktdil.fle:  lfaktdil.lgc
    $(CC) $(CFLAGS) appl=lfaktdil dotfle=$(TD)lfaktdil.fle

$(TD)facrourz.fle:  facrourz.lgc
    $(CC) $(CFLAGS) appl=facrourz dotfle=$(TD)facrourz.fle

$(TD)aca_ascii.fle:  aca_ascii.lgc
    $(CC) $(CFLAGS) appl=aca_ascii dotfle=$(TD)aca_ascii.fle

$(TD)vozmjesta.fle:  vozmjesta.lgc
    $(CC) $(CFLAGS) appl=vozmjesta dotfle=$(TD)vozmjesta.fle

$(TD)vozobrac.fle:  vozobrac.lgc
    $(CC) $(CFLAGS) appl=vozobrac dotfle=$(TD)vozobrac.fle

$(TD)vozponobr.fle:  vozponobr.lgc
    $(CC) $(CFLAGS) appl=vozponobr dotfle=$(TD)vozponobr.fle

$(TD)voz_nzp.fle:  voz_nzp.lgc
    $(CC) $(CFLAGS) appl=voz_nzp dotfle=$(TD)voz_nzp.fle

$(TD)voznzpst.fle:  voznzpst.lgc
    $(CC) $(CFLAGS) appl=voznzpst dotfle=$(TD)voznzpst.fle

$(TD)vnarfiat.fle:  vnarfiat.lgc
    $(CC) $(CFLAGS) appl=vnarfiat dotfle=$(TD)vnarfiat.fle

$(TD)vnfiatst.fle:  vnfiatst.lgc
    $(CC) $(CFLAGS) appl=vnfiatst dotfle=$(TD)vnfiatst.fle

$(TD)lulkzap.fle:  lulkzap.lgc
    $(CC) $(CFLAGS) appl=lulkzap dotfle=$(TD)lulkzap.fle

$(TD)vozizdcs.fle:  vozizdcs.lgc
    $(CC) $(CFLAGS) appl=vozizdcs dotfle=$(TD)vozizdcs.fle

$(TD)fiatfile.fle:  fiatfile.lgc
    $(CC) $(CFLAGS) appl=fiatfile dotfle=$(TD)fiatfile.fle

$(TD)lrazkzap.fle:  lrazkzap.lgc
    $(CC) $(CFLAGS) appl=lrazkzap dotfle=$(TD)lrazkzap.fle

$(TD)lzalkzap.fle:  lzalkzap.lgc
    $(CC) $(CFLAGS) appl=lzalkzap dotfle=$(TD)lzalkzap.fle

$(TD)lizkzap.fle:  lizkzap.lgc
    $(CC) $(CFLAGS) appl=lizkzap dotfle=$(TD)lizkzap.fle

$(TD)vtrosar.fle:  vtrosar.lgc
    $(CC) $(CFLAGS) appl=vtrosar dotfle=$(TD)vtrosar.fle

$(TD)vtrosar_eu.fle:  vtrosar_eu.lgc
    $(CC) $(CFLAGS) appl=vtrosar_eu dotfle=$(TD)vtrosar_eu.fle

$(TD)lspjcduz.fle:  lspjcduz.lgc
    $(CC) $(CFLAGS) appl=lspjcduz dotfle=$(TD)lspjcduz.fle

$(TD)lizvkzap.fle:  lizvkzap.lgc
    $(CC) $(CFLAGS) appl=lizvkzap dotfle=$(TD)lizvkzap.fle

$(TD)vozjcdul.fle:  vozjcdul.lgc
    $(CC) $(CFLAGS) appl=vozjcdul dotfle=$(TD)vozjcdul.fle

$(TD)vozjcdca.fle:  vozjcdca.lgc
    $(CC) $(CFLAGS) appl=vozjcdca dotfle=$(TD)vozjcdca.fle

$(TD)razlkzap.fle:  razlkzap.lgc
    $(CC) $(CFLAGS) appl=razlkzap dotfle=$(TD)razlkzap.fle

$(TD)lotpyugo.fle:  lotpyugo.lgc
    $(CC) $(CFLAGS) appl=lotpyugo dotfle=$(TD)lotpyugo.fle

$(TD)lvnspecd.fle:  lvnspecd.lgc
    $(CC) $(CFLAGS) appl=lvnspecd dotfle=$(TD)lvnspecd.fle

$(TD)lacspecp.fle:  lacspecp.lgc
    $(CC) $(CFLAGS) appl=lacspecp dotfle=$(TD)lacspecp.fle

$(TD)lspdrab.fle:  lspdrab.lgc
    $(CC) $(CFLAGS) appl=lspdrab dotfle=$(TD)lspdrab.fle

$(TD)ljcduzgo.fle:  ljcduzgo.lgc
    $(CC) $(CFLAGS) appl=ljcduzgo dotfle=$(TD)ljcduzgo.fle

$(TD)acnarfiat.fle:  acnarfiat.lgc
    $(CC) $(CFLAGS) appl=acnarfiat dotfle=$(TD)acnarfiat.fle

$(TD)vnstdop.fle:  vnstdop.lgc
    $(CC) $(CFLAGS) appl=vnstdop dotfle=$(TD)vnstdop.fle

$(TD)lvoznarac.fle:  lvoznarac.lgc
    $(CC) $(CFLAGS) appl=lvoznarac dotfle=$(TD)lvoznarac.fle

$(TD)vnfambdg.fle:  vnfambdg.lgc
    $(CC) $(CFLAGS) appl=vnfambdg dotfle=$(TD)vnfambdg.fle

$(TD)lvnfambdg.fle:  lvnfambdg.lgc
    $(CC) $(CFLAGS) appl=lvnfambdg dotfle=$(TD)lvnfambdg.fle

$(TD)drabati.fle:  drabati.lgc
    $(CC) $(CFLAGS) appl=drabati dotfle=$(TD)drabati.fle

$(TD)ldrabati.fle:  ldrabati.lgc
    $(CC) $(CFLAGS) appl=ldrabati dotfle=$(TD)ldrabati.fle

$(TD)ldrabtot.fle:  ldrabtot.lgc
    $(CC) $(CFLAGS) appl=ldrabtot dotfle=$(TD)ldrabtot.fle

$(TD)vozinlst.fle:  vozinlst.lgc
    $(CC) $(CFLAGS) appl=vozinlst dotfle=$(TD)vozinlst.fle

$(TD)vbrnarac.fle:  vbrnarac.lgc
    $(CC) $(CFLAGS) appl=vbrnarac dotfle=$(TD)vbrnarac.fle

$(TD)chgprice.fle:  chgprice.lgc
    $(CC) $(CFLAGS) appl=chgprice dotfle=$(TD)chgprice.fle

$(TD)lchgpric.fle:  lchgpric.lgc
    $(CC) $(CFLAGS) appl=lchgpric dotfle=$(TD)lchgpric.fle

$(TD)lvpotvrd.fle:  lvpotvrd.lgc
    $(CC) $(CFLAGS) appl=lvpotvrd dotfle=$(TD)lvpotvrd.fle

$(TD)voz_pst.fle:  voz_pst.lgc
    $(CC) $(CFLAGS) appl=voz_pst dotfle=$(TD)voz_pst.fle

$(TD)proldvoz.fle:  proldvoz.lgc
    $(CC) $(CFLAGS) appl=proldvoz dotfle=$(TD)proldvoz.fle

$(TD)old_vst.fle:  old_vst.lgc
    $(CC) $(CFLAGS) appl=old_vst dotfle=$(TD)old_vst.fle

$(TD)kontkzap.fle: kontkzap.lgc
    $(CC) $(CFLAGS) appl=kontkzap dotfle=$(TD)kontkzap.fle

$(TD)vozgr_sa.fle: vozgr_sa.lgc
    $(CC) $(CFLAGS) appl=vozgr_sa dotfle=$(TD)vozgr_sa.fle

$(TD)lvozdnev.fle: lvozdnev.lgc
    $(CC) $(CFLAGS) appl=lvozdnev dotfle=$(TD)lvozdnev.fle

$(TD)lbosnatpc.fle: lbosnatpc.lgc
    $(CC) $(CFLAGS) appl=lbosnatpc dotfle=$(TD)lbosnatpc.fle

$(TD)vozsklad.fle: vozsklad.lgc
    $(CC) $(CFLAGS) appl=vozsklad dotfle=$(TD)vozsklad.fle

$(TD)lfvfmbdg.fle: lfvfmbdg.lgc
    $(CC) $(CFLAGS) appl=lfvfmbdg dotfle=$(TD)lfvfmbdg.fle

$(TD)vozfaktdo.fle: vozfaktdo.lgc
    $(CC) $(CFLAGS) appl=vozfaktdo dotfle=$(TD)vozfaktdo.fle

$(TD)vcjenkod.fle: vcjenkod.lgc
    $(CC) $(CFLAGS) appl=vcjenkod dotfle=$(TD)vcjenkod.fle

$(TD)vozila_ps.fle: vozila_ps.lgc
    $(CC) $(CFLAGS) appl=vozila_ps dotfle=$(TD)vozila_ps.fle

$(TD)lkzap_car.fle: lkzap_car.lgc
    $(CC) $(CFLAGS) appl=lkzap_car dotfle=$(TD)lkzap_car.fle

$(TD)vozakcija.fle: vozakcija.lgc
    $(CC) $(CFLAGS) appl=vozakcija dotfle=$(TD)vozakcija.fle

$(TD)vozaddkup.fle: vozaddkup.lgc
    $(CC) $(CFLAGS) appl=vozaddkup dotfle=$(TD)vozaddkup.fle

$(TD)vtros_benzin.fle: vtros_benzin.lgc
    $(CC) $(CFLAGS) appl=vtros_benzin dotfle=$(TD)vtros_benzin.fle

$(TD)vtros_diesel.fle: vtros_diesel.lgc
    $(CC) $(CFLAGS) appl=vtros_diesel dotfle=$(TD)vtros_diesel.fle

$(TD)transcje.fle: transcje.lgc
    $(CC) $(CFLAGS) appl=transcje dotfle=$(TD)transcje.fle

$(TD)trans_voz.fle: trans_voz.lgc
    $(CC) $(CFLAGS) appl=trans_voz dotfle=$(TD)trans_voz.fle

$(TD)transpfak.fle: transpfak.lgc
    $(CC) $(CFLAGS) appl=transpfak dotfle=$(TD)transpfak.fle

$(TD)transotp.fle: transotp.lgc
    $(CC) $(CFLAGS) appl=transotp dotfle=$(TD)transotp.fle

$(TD)transtotp.fle: transtotp.lgc
    $(CC) $(CFLAGS) appl=transtotp dotfle=$(TD)transtotp.fle

$(TD)trans_dest.fle: trans_dest.lgc
    $(CC) $(CFLAGS) appl=trans_dest dotfle=$(TD)trans_dest.fle

$(TD)trans_pri.fle: trans_pri.lgc
    $(CC) $(CFLAGS) appl=trans_pri dotfle=$(TD)trans_pri.fle

$(TD)trans_routes.fle: trans_routes.lgc
    $(CC) $(CFLAGS) appl=trans_routes dotfle=$(TD)trans_routes.fle

$(TD)l_transcje.fle: l_transcje.lgc
    $(CC) $(CFLAGS) appl=l_transcje dotfle=$(TD)l_transcje.fle

$(TD)l_trans_pri.fle: l_trans_pri.lgc
    $(CC) $(CFLAGS) appl=l_trans_pri dotfle=$(TD)l_trans_pri.fle

$(TD)ltrans_routes.fle: ltrans_routes.lgc
    $(CC) $(CFLAGS) appl=ltrans_routes dotfle=$(TD)ltrans_routes.fle

$(TD)l_trans_dest.fle: l_trans_dest.lgc
    $(CC) $(CFLAGS) appl=l_trans_dest dotfle=$(TD)l_trans_dest.fle

$(TD)l_transotp.fle: l_transotp.lgc
    $(CC) $(CFLAGS) appl=l_transotp dotfle=$(TD)l_transotp.fle

$(TD)l_trans_voz.fle: l_trans_voz.lgc
    $(CC) $(CFLAGS) appl=l_trans_voz dotfle=$(TD)l_trans_voz.fle

$(TD)l_transpfak.fle: l_transpfak.lgc
    $(CC) $(CFLAGS) appl=l_transpfak dotfle=$(TD)l_transpfak.fle

$(TD)l_transpfak2.fle: l_transpfak2.lgc
    $(CC) $(CFLAGS) appl=l_transpfak2 dotfle=$(TD)l_transpfak2.fle

$(TD)transtat.fle: transtat.lgc
    $(CC) $(CFLAGS) appl=transtat dotfle=$(TD)transtat.fle

$(TD)l_transtat.fle: l_transtat.lgc
    $(CC) $(CFLAGS) appl=l_transtat dotfle=$(TD)l_transtat.fle

$(TD)trans_burza.fle: trans_burza.lgc
    $(CC) $(CFLAGS) appl=trans_burza dotfle=$(TD)trans_burza.fle

$(TD)l_pvfmbdg.fle: l_pvfmbdg.lgc
    $(CC) $(CFLAGS) appl=l_pvfmbdg dotfle=$(TD)l_pvfmbdg.fle

$(TD)ls_pvfmbdg.fle: ls_pvfmbdg.lgc
    $(CC) $(CFLAGS) appl=ls_pvfmbdg dotfle=$(TD)ls_pvfmbdg.fle

$(TD)salonde.fle: salonde.lgc
    $(CC) $(CFLAGS) appl=salonde dotfle=$(TD)salonde.fle

$(TD)l_pvozs.fle: l_pvozs.lgc
    $(CC) $(CFLAGS) appl=l_pvozs dotfle=$(TD)l_pvozs.fle

$(TD)route_dest.fle: route_dest.lgc
    $(CC) $(CFLAGS) appl=route_dest dotfle=$(TD)route_dest.fle

$(TD)l_route_dest.fle: l_route_dest.lgc
    $(CC) $(CFLAGS) appl=l_route_dest dotfle=$(TD)l_route_dest.fle

$(TD)l_trans_cmr.fle: l_trans_cmr.lgc
    $(CC) $(CFLAGS) appl=l_trans_cmr dotfle=$(TD)l_trans_cmr.fle

$(TD)l_trans_cmrKA.fle: l_trans_cmrKA.lgc
    $(CC) $(CFLAGS) appl=l_trans_cmrKA dotfle=$(TD)l_trans_cmrKA.fle

$(TD)trans_cmr.fle: trans_cmr.lgc
    $(CC) $(CFLAGS) appl=trans_cmr dotfle=$(TD)trans_cmr.fle

$(TD)trans_cmrKA.fle: trans_cmrKA.lgc
    $(CC) $(CFLAGS) appl=trans_cmrKA dotfle=$(TD)trans_cmrKA.fle

$(TD)trans_psklad.fle: trans_psklad.lgc
    $(CC) $(CFLAGS) appl=trans_psklad dotfle=$(TD)trans_psklad.fle

$(TD)ltrans_psklad.fle: ltrans_psklad.lgc
    $(CC) $(CFLAGS) appl=ltrans_psklad dotfle=$(TD)ltrans_psklad.fle

$(TD)voztipsp.fle: voztipsp.lgc
    $(CC) $(CFLAGS) appl=voztipsp dotfle=$(TD)voztipsp.fle

$(TD)vozdealsp.fle: vozdealsp.lgc
    $(CC) $(CFLAGS) appl=vozdealsp dotfle=$(TD)vozdealsp.fle

$(TD)vozkatdo.fle: vozkatdo.lgc
    $(CC) $(CFLAGS) appl=vozkatdo dotfle=$(TD)vozkatdo.fle

$(TD)l_vozkatdo.fle: l_vozkatdo.lgc
    $(CC) $(CFLAGS) appl=l_vozkatdo dotfle=$(TD)l_vozkatdo.fle

$(TD)vozdosin.fle: vozdosin.lgc
    $(CC) $(CFLAGS) appl=vozdosin dotfle=$(TD)vozdosin.fle

$(TD)l_vozdosin.fle: l_vozdosin.lgc
    $(CC) $(CFLAGS) appl=l_vozdosin dotfle=$(TD)l_vozdosin.fle

$(TD)shcdodop.fle: shcdodop.lgc
    $(CC) $(CFLAGS) appl=shcdodop dotfle=$(TD)shcdodop.fle

$(TD)l_shcdodop.fle: l_shcdodop.lgc
    $(CC) $(CFLAGS) appl=l_shcdodop dotfle=$(TD)l_shcdodop.fle

$(TD)fmbdgweb.fle: fmbdgweb.lgc
    $(CC) $(CFLAGS) appl=fmbdgweb dotfle=$(TD)fmbdgweb.fle

$(TD)homo_var.fle: homo_var.lgc
    $(CC) $(CFLAGS) appl=homo_var dotfle=$(TD)homo_var.fle

$(TD)salonoff.fle: salonoff.lgc
    $(CC) $(CFLAGS) appl=salonoff dotfle=$(TD)salonoff.fle

$(TD)salonxls.fle: salonxls.lgc
    $(CC) $(CFLAGS) appl=salonxls dotfle=$(TD)salonxls.fle

$(TD)lprimcszg.fle: lprimcszg.lgc
    $(CC) $(CFLAGS) appl=lprimcszg dotfle=$(TD)lprimcszg.fle

$(TD)vrs_nzk.fle: vrs_nzk.lgc
    $(CC) $(CFLAGS) appl=vrs_nzk dotfle=$(TD)vrs_nzk.fle

$(TD)nktipknj.fle: nktipknj.lgc
    $(CC) $(CFLAGS) appl=nktipknj dotfle=$(TD)nktipknj.fle

$(TD)spec_ob14.fle: spec_ob14.lgc
    $(CC) $(CFLAGS) appl=spec_ob14 dotfle=$(TD)spec_ob14.fle

$(TD)spec_ob14z.fle: spec_ob14z.lgc
    $(CC) $(CFLAGS) appl=spec_ob14z dotfle=$(TD)spec_ob14z.fle

$(TD)util_nzk.fle: util_nzk.lgc
    $(CC) $(CFLAGS) appl=util_nzk dotfle=$(TD)util_nzk.fle

$(TD)nzk_ob14.fle: nzk_ob14.lgc
    $(CC) $(CFLAGS) appl=nzk_ob14 dotfle=$(TD)nzk_ob14.fle

$(TD)nzk_wbspec.fle: nzk_wbspec.lgc
    $(CC) $(CFLAGS) appl=nzk_wbspec dotfle=$(TD)nzk_wbspec.fle

$(TD)l_nzk.fle: l_nzk.lgc
    $(CC) $(CFLAGS) appl=l_nzk dotfle=$(TD)l_nzk.fle

$(TD)l_nzk_roba.fle: l_nzk_roba.lgc
    $(CC) $(CFLAGS) appl=l_nzk_roba dotfle=$(TD)l_nzk_roba.fle

$(TD)salonfak.fle: salonfak.lgc
    $(CC) $(CFLAGS) appl=salonfak dotfle=$(TD)salonfak.fle

$(TD)nzk_inousl.fle: nzk_inousl.lgc
    $(CC) $(CFLAGS) appl=nzk_inousl dotfle=$(TD)nzk_inousl.fle

$(TD)voz_napl.fle: voz_napl.lgc
    $(CC) $(CFLAGS) appl=voz_napl dotfle=$(TD)voz_napl.fle

$(TD)voz_kup_ascii.fle: voz_kup_ascii.lgc
    $(CC) $(CFLAGS) appl=voz_kup_ascii dotfle=$(TD)voz_kup_ascii.fle

$(TD)nzk_coun.fle: nzk_coun.lgc
    $(CC) $(CFLAGS) appl=nzk_coun dotfle=$(TD)nzk_coun.fle

$(TD)vozuzak.fle: vozuzak.lgc
    $(CC) $(CFLAGS) appl=vozuzak dotfle=$(TD)vozuzak.fle

$(TD)vozizak.fle: vozizak.lgc
    $(CC) $(CFLAGS) appl=vozizak dotfle=$(TD)vozizak.fle

$(TD)nzk.fle: nzk.lgc
    $(CC) $(CFLAGS) appl=nzk dotfle=$(TD)nzk.fle

$(TD)okzap_car.fle: okzap_car.lgc
    $(CC) $(CFLAGS) appl=okzap_car dotfle=$(TD)okzap_car.fle

$(TD)acpv_idx.fle: acpv_idx.lgc
    $(CC) $(CFLAGS) appl=acpv_idx dotfle=$(TD)acpv_idx.fle

$(TD)lkzap_kont.fle: lkzap_kont.lgc
    $(CC) $(CFLAGS) appl=lkzap_kont dotfle=$(TD)lkzap_kont.fle

$(TD)voz_finlag.fle: voz_finlag.lgc
    $(CC) $(CFLAGS) appl=voz_finlag dotfle=$(TD)voz_finlag.fle

$(TD)l_finlag.fle: l_finlag.lgc
    $(CC) $(CFLAGS) appl=l_finlag dotfle=$(TD)l_finlag.fle

$(TD)lpon_finlag.fle: lpon_finlag.lgc
    $(CC) $(CFLAGS) appl=lpon_finlag dotfle=$(TD)lpon_finlag.fle

$(TD)finlag_pon.fle: finlag_pon.lgc
    $(CC) $(CFLAGS) appl=finlag_pon dotfle=$(TD)finlag_pon.fle

############## DEALERI
#
$(TD)vozdepr.fle:  vozdepr.lgc
    $(CC) $(CFLAGS) appl=vozdepr dotfle=$(TD)vozdepr.fle

$(TD)vozsalon.fle:  vozsalon.lgc
    $(CC) $(CFLAGS) appl=vozsalon dotfle=$(TD)vozsalon.fle

$(TD)vozsalag.fle:  vozsalag.lgc
    $(CC) $(CFLAGS) appl=vozsalag dotfle=$(TD)vozsalag.fle

$(TD)ppardeal.fle:  ppardeal.lgc
    $(CC) $(CFLAGS) appl=ppardeal dotfle=$(TD)ppardeal.fle

$(TD)vozdelag.fle:  vozdelag.lgc
    $(CC) $(CFLAGS) appl=vozdelag dotfle=$(TD)vozdelag.fle

$(TD)vozexcel.fle:  vozexcel.lgc
    $(CC) $(CFLAGS) appl=vozexcel dotfle=$(TD)vozexcel.fle

$(TD)azur_stav.fle: azur_stav.lgc
    $(CC) $(CFLAGS) appl=azur_stav dotfle=$(TD)azur_stav.fle

$(TD)chg_ordine.fle: chg_ordine.lgc
    $(CC) $(CFLAGS) appl=chg_ordine dotfle=$(TD)chg_ordine.fle

$(TD)lkzapkn_car.fle: lkzapkn_car.lgc
    $(CC) $(CFLAGS) appl=lkzapkn_car dotfle=$(TD)lkzapkn_car.fle

$(TD)lkzapeur_car.fle: lkzapeur_car.lgc
    $(CC) $(CFLAGS) appl=lkzapeur_car dotfle=$(TD)lkzapeur_car.fle

$(TD)dkzap_car.fle: dkzap_car.lgc
    $(CC) $(CFLAGS) appl=dkzap_car dotfle=$(TD)dkzap_car.fle

$(TD)crediflex.fle: crediflex.lgc
    $(CC) $(CFLAGS) appl=crediflex dotfle=$(TD)crediflex.fle

$(TD)cflimiti.fle: cflimiti.lgc
    $(CC) $(CFLAGS) appl=cflimiti dotfle=$(TD)cflimiti.fle

$(TD)l_ispod_nab.fle: l_ispod_nab.lgc
    $(CC) $(CFLAGS) appl=l_ispod_nab dotfle=$(TD)l_ispod_nab.fle

$(TD)l_voz_co2.fle: l_voz_co2.lgc
    $(CC) $(CFLAGS) appl=l_voz_co2 dotfle=$(TD)l_voz_co2.fle

$(TD)ctar_hist.fle: ctar_hist.lgc
    $(CC) $(CFLAGS) appl=ctar_hist dotfle=$(TD)ctar_hist.fle

$(TD)vozupdtc.fle: vozupdtc.lgc
    $(CC) $(CFLAGS) appl=vozupdtc dotfle=$(TD)vozupdtc.fle

$(TD)vrste_prodaje.fle: vrste_prodaje.lgc
    $(CC) $(CFLAGS) appl=vrste_prodaje dotfle=$(TD)vrste_prodaje.fle

$(TD)opisnik.fle: opisnik.lgc
    $(CC) $(CFLAGS) appl=opisnik dotfle=$(TD)opisnik.fle

$(TD)eucjenik.fle: eucjenik.lgc
    $(CC) $(CFLAGS) appl=eucjenik dotfle=$(TD)eucjenik.fle

$(TD)skupine_op.fle: skupine_op.lgc
    $(CC) $(CFLAGS) appl=skupine_op dotfle=$(TD)skupine_op.fle

$(TD)cszg_kalk.fle: cszg_kalk.lgc
    $(CC) $(CFLAGS) appl=cszg_kalk dotfle=$(TD)cszg_kalk.fle

$(TD)cszg_kalks.fle: cszg_kalks.lgc
    $(CC) $(CFLAGS) appl=cszg_kalks dotfle=$(TD)cszg_kalks.fle

$(TD)lotpvoz_eu.fle: lotpvoz_eu.lgc
    $(CC) $(CFLAGS) appl=lotpvoz_eu dotfle=$(TD)lotpvoz_eu.fle

$(TD)lotpmalp_eu.fle: lotpmalp_eu.lgc
    $(CC) $(CFLAGS) appl=lotpmalp_eu dotfle=$(TD)lotpmalp_eu.fle

$(TD)ltrosarine.fle: ltrosarine.lgc
    $(CC) $(CFLAGS) appl=ltrosarine dotfle=$(TD)ltrosarine.fle

$(TD)acpv_rev.fle:  acpv_rev.lgc
    $(CC) $(CFLAGS) appl=acpv_rev dotfle=$(TD)acpv_rev.fle

$(TD)salon.fle:  salon.lgc
    $(CC) $(CFLAGS) appl=salon dotfle=$(TD)salon.fle

$(TD)salnarfiat.fle:  salnarfiat.lgc
    $(CC) $(CFLAGS) appl=salnarfiat dotfle=$(TD)salnarfiat.fle

$(TD)l_ngo.fle:  l_ngo.lgc
    $(CC) $(CFLAGS) appl=l_ngo dotfle=$(TD)l_ngo.fle

$(TD)chg_kodeks.fle:  chg_kodeks.lgc
    $(CC) $(CFLAGS) appl=chg_kodeks dotfle=$(TD)chg_kodeks.fle

$(TD)l_rabcjenik.fle:  l_rabcjenik.lgc
    $(CC) $(CFLAGS) appl=l_rabcjenik dotfle=$(TD)l_rabcjenik.fle

$(TD)l_eucjenik.fle:  l_eucjenik.lgc
    $(CC) $(CFLAGS) appl=l_eucjenik dotfle=$(TD)l_eucjenik.fle

$(TD)l_eucjenik8.fle:  l_eucjenik8.lgc
    $(CC) $(CFLAGS) appl=l_eucjenik8 dotfle=$(TD)l_eucjenik8.fle

$(TD)l_eucjenik7.fle:  l_eucjenik7.lgc
    $(CC) $(CFLAGS) appl=l_eucjenik7 dotfle=$(TD)l_eucjenik7.fle

$(TD)l_eucjenik6.fle:  l_eucjenik6.lgc
    $(CC) $(CFLAGS) appl=l_eucjenik6 dotfle=$(TD)l_eucjenik6.fle

$(TD)l_eucjenik5.fle:  l_eucjenik5.lgc
    $(CC) $(CFLAGS) appl=l_eucjenik5 dotfle=$(TD)l_eucjenik5.fle

$(TD)l_eucjenik4.fle:  l_eucjenik4.lgc
    $(CC) $(CFLAGS) appl=l_eucjenik4 dotfle=$(TD)l_eucjenik4.fle

$(TD)l_eucjenik10.fle:  l_eucjenik10.lgc
    $(CC) $(CFLAGS) appl=l_eucjenik10 dotfle=$(TD)l_eucjenik10.fle

$(TD)l_eucjenik11.fle:  l_eucjenik11.lgc
    $(CC) $(CFLAGS) appl=l_eucjenik11 dotfle=$(TD)l_eucjenik11.fle

$(TD)l_jamstvo5.fle:  l_jamstvo5.lgc
    $(CC) $(CFLAGS) appl=l_jamstvo5 dotfle=$(TD)l_jamstvo5.fle

$(TD)tros_xml.fle:  tros_xml.lgc
    $(CC) $(CFLAGS) appl=tros_xml dotfle=$(TD)tros_xml.fle

$(TD)prodavaci.fle:  prodavaci.lgc
    $(CC) $(CFLAGS) appl=prodavaci dotfle=$(TD)prodavaci.fle

$(TD)chkcijene.fle:  chkcijene.lgc
    $(CC) $(CFLAGS) appl=chkcijene dotfle=$(TD)chkcijene.fle

$(TD)chkcijene_st.fle:  chkcijene_st.lgc
    $(CC) $(CFLAGS) appl=chkcijene_st dotfle=$(TD)chkcijene_st.fle

$(TD)boje.fle:  boje.lgc
    $(CC) $(CFLAGS) appl=boje dotfle=$(TD)boje.fle

$(TD)tekstovi.fle:  tekstovi.lgc
    $(CC) $(CFLAGS) appl=tekstovi dotfle=$(TD)tekstovi.fle

$(TD)vmarke.fle:  vmarke.lgc
    $(CC) $(CFLAGS) appl=vmarke dotfle=$(TD)vmarke.fle

